require 'byebug'
class RPNCalculator
  # TODO: your code goes here!
  # @@stack = []
  # @@value = 0.0

  def initialize
    @@stack = []
    @@value = 0.0
  end

  def value
    @@value
  end

  def push(num)
    @@stack.unshift(num)
  end

  def pop
    @@stack.shift
  end

  def plus
    raise_error if @@stack.length < 2
    @@value = pop + @@stack[0]
    @@stack[0] = @@value if @@stack.length >= 1
  end

  def minus
    raise_error if @@stack.length < 2
    x = pop
    @@value = @@stack[0] - x
    @@stack[0] = @@value if @@stack.length >= 1
  end

  def divide
    raise_error if @@stack.length < 2
    x = pop
    @@value = @@stack[0].to_f / x
    @@stack[0] = @@value if @@stack.length >= 1
  end

  def times
    raise_error if @@stack.length < 2
    @@value = pop * @@stack[0]
    @@stack[0] = @@value if @@stack.length >= 1
  end

  def tokens(str)
    arr = []
    numbers = "0123456789"
    str.each_char do |ch|
      if ch == " "
        next
      elsif numbers.include?(ch)
        arr << ch.to_i
      else
        arr << ch.to_sym
      end
    end
    arr
  end

  def evaluate(str)
    arr = tokens(str)
    arr.each do |el|
      if el.class == Fixnum
        push(el)
      else
        case el
        when :+
          plus
        when :-
          minus
        when :*
          times
        when :/
          divide
        end
      end
    end
    @@value
  end

  def raise_error
    raise "calculator is empty"
  end

end
